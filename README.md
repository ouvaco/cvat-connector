## CVAT API Client Library

[![license](https://img.shields.io/badge/license-Apache%202-blue)](LICENSE)

This is a client library to interact with CVAT Server.

Not everything is implemented at the moment.

### Usage

Create a new client instance with CVAT Server URL

```python
from cvat_connector import CVATConnector

cvat_client = CVATConnector("https://cvat.mydomain.com")
```

Login to with your credentials

```python
cvat_client.login("myuser", "passw0rd")
```

Fetch users etc.

```python
users = cvat_client.get_all_users()
for username, user_details in users.items():
	print(f"{username} id is {user_details['id']}")
```

Logout when you are done.

```python
cvat_client.logut()
```